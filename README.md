# Flask Shop - SALEOR


## Introduction
This project is a front page copy of [saleor](https://github.com/mirumee/saleor) old version, written with Flask. 

**Live demo : https://flaskshopsale.herokuapp.com/**

## Built With

- Python 3.8.5
- PostgreSQL / MySQL
- Flask
- Redis
- Elasticsearch


## ScreenShot

<table align="center">
    <tr>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/1.png">
                <img src="https://gitlab.com/danthuyvo.dev/flask-shop/-/raw/main/ScreenShot/1.png" alt="Screenshot Home" width="300px" />
            </a>
        </td>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/2.png">
                <img src="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/2.png" alt="Screenshot Category" width="300px" />
            </a>
        </td>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/3.png">
                <img src="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/3.png" alt="Screenshot Cart" width="300px" />
            </a>
        </td>
    </tr>
    <tr>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/4.png">
                <img src="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/4.png" alt="Screenshot Admin Panel" width="300px" />
            </a>
        </td>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/5.png">
                <img src="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/5.png" alt="Screenshot Site Configuration" width="300px" />
            </a>
        </td>
        <td align="center">
            <a href="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/6.png">
                <img src="https://raw.githubusercontent.com/hjlarry/flask-shop/master/ScreenShot/6.png" alt="Screenshot Order List" width="300px" />
            </a>
        </td>
    </tr>
</table>



**Features**

- **Headless / API first**: Build mobile apps, custom storefronts, POS, automation, etc
- **Extensible**: Build anything with webhooks, apps, metadata, and attributes
- **Multichannel**: Per channel control of pricing, currencies, stock, product, and more
- **Enterprise ready**: Secure, scalable, and stable. Battle-tested by big brands
- **Dashboard**: User friendly, fast and productive. (Decoupled project repo )
- **Orders**: A comprehensive system for orders, dispatch, and refunds
- **Cart**: Advanced payment and tax options, with full control over discounts and promotions
- **Payments**: Flexible API architecture allows integration of any payment method
- **SEO**: Packed with features that get stores to a wider audience
- **Cloud**: Optimized for deployments using Docker



## Quickstart

### Use python virtual environment

**First, Clone and Install dependence**
```
git clone https://github.com/hjlarry/flask-shop.git
cd flask-shop
python3 -m venv .venv
# on windows, you should run .venv\Scripts\activate.bat 
source .venv/bin/activate
pip3 install -r requirements.txt
```

**Second, Init db and run**
```
# modify .flaskenv and flaskshop/setting.py
flask createdb
flask seed
flask run
```

### Use Docker 
**First, Build image and run in background**
```
docker-compose up -d
```
**Second, enter container and add fake data**
```
docker-compose exec web sh
flask createdb
flask seed
```



## Settings
If you want change default settings create file .flaskenv with content:
```
FLASK_APP=autoapp.py
FLASK_ENV=develop
FLASK_DEBUG=1
FLASK_RUN_HOST=0.0.0.0
FLASK_RUN_PORT=5000
SECRET_KEY=abcdefgh
DB_TYPE = postgresql # mysql
DB_USER = root
DB_PASSWD = my_passwd
DB_HOST = 127.0.0.1
DB_PORT = 5432
BABEL_DEFAULT_LOCALE = en_US
BABEL_DEFAULT_TIMEZONE = UTC
BABEL_TRANSLATION_DIRECTORIES = ../translations
BABEL_CURRENCY = USD
GA_MEASUREMENT_ID = G-QCX3G9KSPC
``` 



If the js files has been modified, you need to:
```
npm install
npm run build
```

Redis and Elasticsearch is unabled by default, You can enable them for good performence and search ablitity.

Source : https://github.com/hjlarry/

